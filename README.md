# Introduction
I know there are probably people much better than me out there who have a template for doing Advent of Code in Java, but
this is more for myself to save time in the future, that I'll be able to just clone this repo and begin immediately
rather than working on this first before the problems. On the off chance that I forget how this works, or someone else
uses it, the instructions are below.

# How to use
1. Input goes into an ignore folder in/ the root of the project. Days inputs go in subdirectories in the naming scheme xx, where xx is the day of Advent in two character format.  
   i.e. ``in/01`` contains the input for Day 1 while ``in/10`` contains the input for Day 10.
2. Code goes in a subpackage of the AoC package named dayxx (where xx is again the relevent day in two Char form).
3. Make sure to expand the switch statement as you do days
4. Have fun!!

# Sources
* Java code
    * The many tutorials on console scanning and file handling
* Nix flake
    * https://github.com/the-nix-way/dev-templates