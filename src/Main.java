import AoC.Driver;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Start and end times for the whole program
        long startTime, endTime;

        //Which data
        boolean test;
        //Input and Input gathering
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome to Advent of Code 2023!!");
        System.out.println("What day are you doing?");
        int day = sc.nextInt();
        sc.nextLine();
        System.out.println("Do you want to use test data?");
        test = sc.nextLine().equals("yes");
        sc.close();

        //Only sart timing after last input
        startTime = System.currentTimeMillis();
        Driver runner = new Driver(day, test);

        //Begin the program
        runner.start();

        //End of the program
        endTime = System.currentTimeMillis();
        System.out.printf("This program took %dms to complete", (endTime - startTime));
    }
}