package AoC;

import AoC.day01.Day1Part1;
import AoC.day01.Day1Part2;

import AoC.day02.*;

import java.util.ArrayList;

public class Driver {

    //Day being done
    private int day;

    //File path
    private String filePath;

    private boolean duckThisItsLate = false;
    private ArrayList<String> input;

    /**
     * Constructor for this class
     *
     * @param day  - Day being done
     * @param test - Test data or actual data
     */
    public Driver(int day, boolean test) {
        this.day = day;
        //If the day is < than 10, preappend 0 to the number, if not, add the number
        if (this.day < 10) {
            this.filePath = "./in/0" + this.day + "/";
        } else {
            this.filePath = "./in/" + this.day + "/";
        }

        //Which file is in use
        if (test) {
            this.filePath += "test.txt";
        } else {
            this.filePath += "data.txt";
        }
    }

    public void start() {
        //Day label
        System.out.printf("Day %d\n", this.day);
        try {
            FileHandler file = new FileHandler(this.filePath);
            long startTime, endTime;

            switch (this.day) {
                default:
                    System.out.println("Invalid day");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
