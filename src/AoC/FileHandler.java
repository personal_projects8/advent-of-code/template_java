package AoC;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileHandler {
    ArrayList<String> input = new ArrayList<>();
    public FileHandler(String filePath) throws FileNotFoundException {
        File input = new File(filePath);
        Scanner myReader = new Scanner(input);
        while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            this.input.add(data);
        }
        myReader.close();
    }

    public ArrayList<String> getData(){
        return this.input;
    }
}
